#include <iostream>
#include <string>
#include <cstdlib>
#include <algorithm>
#include <time.h>

using namespace std;

/**
 * Programming Challenge: Heads/Tails
 * by Sean Castillo 2019
 */

int main()
{
    string input;
    unsigned int coinFlip, userAnswer;

    //game loop
    loopLabel:

    //flip the coin
    srand(time(NULL));
    coinFlip = rand() % 2;

    //get the user's answer
    cout << "Heads (1) or Tails (0) ?" << endl;
    cin >> input;

    //if the input is valid, compare the answer with the coin flip
    if (input == "1" || input == "0" || input == "heads" || input == "tails")
    {
        if (input == "1" || input == "heads") { userAnswer = 1; }
        else                                  { userAnswer = 0; }

        if (coinFlip == userAnswer) { cout << "YES! YES! YES!" << endl; }
        else                        { cout << "NO! NO! NO!"    << endl; }

        //ask the user whether to play again
        againLoop:
        cout << "Again? (Y)es or (N)o?" << endl;
        cin >> input;
        std::for_each(input.begin(), input.end(), [](char & c)
        {   c = ::tolower(c); });
        if      (input == "y" || input == "yes")
        {       goto loopLabel;                }
        else if (input == "n" || input == "no")
        {
            cout << "The game has ended." << endl;
        }
        else
        {
            cout << "Incorrect input! Must be (Y)es or (N)o!" << endl;
            cin.clear();
            cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            goto againLoop;
        }

    //if the input is invalid, just loop back
    }
    else
    {
        cout << "Incorrect input! Must be 1/Heads or 0/Tails!" << endl;
        cin.clear();
        cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        goto loopLabel;
    }

    return 0;
}
